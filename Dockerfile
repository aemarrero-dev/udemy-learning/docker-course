FROM centos:latest
LABEL version = 1.0
LABEL description = "Curso Docker Udemy"
RUN yum install httpd -y
RUN echo "$(whoami)" > /var/www/html/user1.html
CMD apachectl -DFOREGROUND
