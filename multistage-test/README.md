
## USO DE MULTISTAGE
El multistage nos permite agregar varios FROM dentro del Dockerfile. El ultimo FROM es el que es usado para crear la imagen resultado.

En el archivo Dockerfile:

1. Usamos una IMAGEN de Maven para construir un ejecutable jar de una app springboot basica llamada demo.
2. Luego ejecutamos el jar resultante en una IMAGEN java/jdk alpine

En el archivo Dockerfile2:
1. Creamos 3 archivos de distinto peso para demostrar el multistage. 
2. La IMAGEN de centos pesa solo 200MB pero con los 3 archivos llega a 260MB aproximado.
3. Imagen resultado tiene un peso menor, evidenciando que el primer FROM solo es procesado pero no tomado en cuenta para la imagen resultante.