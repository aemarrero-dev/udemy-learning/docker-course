## APACHE CON SSL. (HTTPS)

Creamos los certificados SSL (file.key, file.crt)  con el siguiente comando:

### `openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout docker.key -out docker.crt`

Omitimos las preguntas del proceso (country name, organization, etc.) con Enter y solo especificamos el valor de localhost en Common Name.

Como resultado obtendremos docker.key y docker.crt que son los certificados SSL que serán instalados en el servidor.

## CONFIGURACION DE APACHE PARA USAR SSL (HTTPS)

Archivo ssl.conf

```
<VirtualHost *:443>
 ServerName localhost
 DocumentRoot /var/www/html
 SSLEngine on
 SSLCertificateFile /docker.crt
 SSLCertificateKeyFile /docker.key
</VirtualHost>

```




